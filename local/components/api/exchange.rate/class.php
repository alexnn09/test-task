<?

use Core\ExchangeHelper;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class ExampleCompSimple extends CBitrixComponent
{
    const REQUIRED_PARAMETERS = 'The required parameters are missing';
    private $fields = [];

    public function executeComponent()
    {
        if (!self::checkRequiredFields()) {
            self::output(ExchangeHelper::$errorRequiredFields);
        }
        self::checkTable();
        self::output(ExchangeHelper::responseProcessing($this->fields));
    }

    private function checkRequiredFields()
    {
        $input = json_decode(file_get_contents("php://input"), 1);
        if ($input["date"] && is_array($input["currency"])) {
            $this->fields = $input;
        }
        return (bool)$this->fields;
    }

    private function checkTable()
    {
        $currencyTable = new \Tables\ExchangeRateTable();
        $connection = \Bitrix\Main\Application::getConnection();
        if (!$connection->isTableExists($currencyTable::getTableName())) {
            $currencyTable::getEntity()->createDbTable();
            $currencyTable::onBeforeTableCreate();
        }
    }

    private function output($response)
    {
        if ($response["status"] == "error") http_response_code(400);
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($response);
        die;
    }
}