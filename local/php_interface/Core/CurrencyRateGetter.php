<?

namespace Core;

use Bitrix\Main\Type\DateTime;

class CurrencyRateGetter
{
    const BASE_URL = "https://www.cbr-xml-daily.ru/daily_json.js";
    const START_URL_OLD = "https://www.cbr-xml-daily.ru/archive/";
    const END_URL_OLD = '/daily_json.js';

    public function getCurrency()
    {
        return self::response();
    }

    public function getOldCurrency($date)
    {
        return self::response(self::START_URL_OLD . implode($date, "/") . self::END_URL_OLD);
    }

    private function response($url = self::BASE_URL)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return self::dataProcessing($response);
    }

    private function dataProcessing($response)
    {
        $data = [];
        $response = json_decode($response, true);
        if (new \DateTime($response["PreviousDate"]) < new DateTime()) {
            $date = DateTime::createFromPhp((new \DateTime($response["PreviousDate"]))
                ->setTime(0, 0, 0));
        } else {
            $date = DateTime::createFromPhp((new \DateTime($response["Date"]))
                ->setTime(0, 0, 0));
        }

        foreach ($response["Valute"] as $item) {
            $data[] = [
                "DATE" => $date,
                "CURRENCY_NAME" => (string)$item["Name"],
                "CURRENCY_CODE" => (string)$item["CharCode"],
                "CURRENCY_NOMINAL" => (int)$item["Nominal"],
                "VALUE" => (float)$item["Previous"]
            ];
        }
        return $data;
    }
}