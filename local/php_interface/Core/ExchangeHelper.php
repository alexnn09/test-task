<?

namespace Core;

use Bitrix\Main\Type\DateTime,
    Tables\ExchangeRateTable;

class ExchangeHelper
{
    const REQUIRED_PARAMETERS = 'The required parameters are incorrect';

    public static $errorRequiredFields = [
        "status" => "error",
        "msg" => "The required parameters are missing"
    ];

    private function add($fields)
    {
        return ExchangeRateTable::add($fields);
    }

    private function update($id, $fields)
    {
        return ExchangeRateTable::update($id, $fields);
    }

    private function delete($id)
    {
        return ExchangeRateTable::delete($id);
    }

    public static function responseProcessing($data)
    {
        $response = [
            "status" => "",
            "msg" => "",
            "data" => []
        ];

        $date = \DateTime::createFromFormat('d-m-Y', $data["date"])->setTime(0, 0, 0);
        if (!$date || $date > \DateTime::createFromFormat('d-m-Y', date('d-m-Y'))
        ) {
            $date = null;
        }
        if ($date) {
            $response["status"] = "OK";
            $response["data"]["date"] = $date->format('d-m-Y');
            $res = ExchangeRateTable::getList(
                [
                    "filter" => [
                        "DATE" => DateTime::createFromPhp($date),
                        "CURRENCY_CODE" => $data["currency"]
                    ]
                ]);
            if ($rows = $res->fetchAll()) {

                $response["currencies"] = self::compileResult($rows);
            } else {
                $response["currencies"] = self::compileResult(self::getNewDay($date, $data["currency"]));
            }
        } else {
            $response["status"] = "error";
            $response["msg"] = self::REQUIRED_PARAMETERS;
        }
        return $response;
    }

    private static function compileResult($rows)
    {
        $data = [];
        foreach ($rows as $row) {
            $row["DATE"]->format('d-m-Y');
            $data[] = [
                "currency_name" => $row["CURRENCY_NAME"],
                "currency_code" => $row["CURRENCY_CODE"],
                "nominal" => $row["CURRENCY_NOMINAL"],
                "value" => $row["VALUE"]
            ];
        }
        return $data;
    }

    private static function getNewDay(\DateTime $date, $currencies)
    {
        $data = (new CurrencyRateGetter())->getOldCurrency(
            [
                $date->format('Y'),
                $date->format('m'),
                $date->format('d')
            ]
        );
        foreach ($data as $item) {
            self::add($item);
        }
        return self::extractCurrencies($data, $currencies);
    }

    private static function extractCurrencies($data, $currencies)
    {
        foreach ($data as $key => $item) {
            if (!in_array($item["CURRENCY_CODE"], $currencies)) {
                unset($data[$key]);
            }
        }
        return $data;
    }
}