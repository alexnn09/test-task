<?

namespace Tables;

use Bitrix\Main,
    Bitrix\Main\ORM,
    Core\CurrencyRateGetter;

class ExchangeRateTable extends ORM\Data\DataManager
{
    public static function getTableName()
    {
        return "api_exchange_rate";
    }

    public static function getMap()
    {
        return array(
            "ID" => new ORM\Fields\IntegerField("ID", [
                'primary' => true,
                'autocomplete' => true,
            ]),
            "DATE" => new ORM\Fields\DatetimeField("DATE", [
                'required' => true,
            ]),
            "CURRENCY_NAME" => new ORM\Fields\StringField("CURRENCY_NAME", [
                'required' => true,
            ]),
            "CURRENCY_CODE" => new ORM\Fields\StringField("CURRENCY_CODE", [
                'required' => true,
            ]),
            "CURRENCY_NOMINAL" => new ORM\Fields\IntegerField("CURRENCY_NOMINAL", [
                'required' => true,
            ]),
            "VALUE" => new ORM\Fields\FloatField("VALUE", [
                'required' => true,
            ])
        );
    }

    public static function onBeforeTableCreate()
    {
        foreach ((new CurrencyRateGetter())->getCurrency() as $item) {
            self::add($item);
        }
    }
}