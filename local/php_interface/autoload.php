<?
Bitrix\Main\Loader::registerAutoLoadClasses(null, [
    "Tables\ExchangeRateTable" => "/local/php_interface/Tables/ExchangeRateTable.php",
    "Core\CurrencyRateGetter" => "/local/php_interface/Core/CurrencyRateGetter.php",
    "Core\ExchangeHelper" => "/local/php_interface/Core/ExchangeHelper.php"
]);